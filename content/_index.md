+++
title = "Home Page"
template = "index.html"
+++


<div style="padding-bottom:56.25%; position:relative; display:block; width: 100%">
  <iframe width="100%" height="100%"
    src="https://www.veed.io/embed/ef22eb6f-c398-43a5-a7f0-7c03418ac1fe" 
    frameborder="0"  style="position:absolute; top:0; left: 0" title="video (1080p)" webkitallowfullscreen mozallowfullscreen allowfullscreen>
  </iframe>
</div>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Join Waitlist Button</title>
<style>
  .button {
    position: relative;
    display: inline-block;
    padding: 10px 30px;
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    text-decoration: none;
    background-color: #000000; /* Black */
    color: white;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    transition: background-color 0.3s;
    overflow: hidden;
  }

  .button:hover {
    background-color: #333333; /* Darker Black */
  }

  .button::after {
    content: '';
    position: absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%);
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 10px 0 10px 10px;
    border-color: transparent transparent transparent white;
  }

  .container {
    text-align: right;
    padding: 20px;
  }
</style>
</head>
<body>

<div class="container">
  <a href="/about" class="button">Join Waitlist</a>
</div>

</body>
</html>
